package com.kshrd.btbrestfulapidemo.controller.restcontroller;


import com.kshrd.btbrestfulapidemo.model.UserResponse;
import com.kshrd.btbrestfulapidemo.model.request.UserLoginRequest;
import com.kshrd.btbrestfulapidemo.model.response.UserLoginResponse;
import com.kshrd.btbrestfulapidemo.repository.StudentRepository;
import com.kshrd.btbrestfulapidemo.security.UserDetailImp;
import com.kshrd.btbrestfulapidemo.security.jwt.JwtUtils;
import com.kshrd.btbrestfulapidemo.utils.reponse.Response;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthRestController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    StudentRepository studentRepository;

    ModelMapper modelMapper = new ModelMapper();

    @PostMapping("/login")
    Response<UserLoginResponse>login (@RequestBody UserLoginRequest request){

        UsernamePasswordAuthenticationToken  usernamePasswordAuthenticationToken = new
                UsernamePasswordAuthenticationToken(
                        request.getUsername(),request.getPassword()
        );
        try{

            Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);

            JwtUtils jwtUtils = new JwtUtils();
            String token = jwtUtils.generateJwtToken(authentication);

            UserLoginResponse response = new UserLoginResponse();

            System.out.println("Here is the value of the token : "+token);

            try{
                UserResponse findUserByUsername =studentRepository.findByUsername(request.getUsername());
               // response.setUsername(findUserByUsername.getUsername());
                response = modelMapper.map(findUserByUsername,UserLoginResponse.class);
                response.setToken(token);
                return Response.<UserLoginResponse>ok().setPayload(response).setError("Successfully login....");

            }catch (Exception ex){
                System.out.println("Fialed to find the user with the provided id: "+ex.getMessage());
                return Response.<UserLoginResponse>exception().setError("Cannot find user by the given username");
            }
        }catch (Exception exception){

            System.out.println("Error when user trying to log in : "+ exception.getMessage());
            return Response.<UserLoginResponse>exception().setError("Failed to login , exception occur");

        }

    }


    @GetMapping("/check")
    String checkToken (@AuthenticationPrincipal UserDetailImp login){

        try {

            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            System.out.println("Here is the value of the login User : ");
            System.out.println(login);

            if (login.getAuthorities().contains("USER")){
                return "You are the User";
            }else {
                return "You are not the User ";
            }


        }catch (Exception ex){

            System.out.println("Error when authenticate the user : "+ex.getMessage());
            return "Error";
        }

    }
}
