package com.kshrd.btbrestfulapidemo.controller.restcontroller;

import com.kshrd.btbrestfulapidemo.dto.RegisterDTO;
import com.kshrd.btbrestfulapidemo.mapper.StudentMapper;
import com.kshrd.btbrestfulapidemo.model.Student;
import com.kshrd.btbrestfulapidemo.service.StudentService;
import com.kshrd.btbrestfulapidemo.utils.paging.Paging;
import com.kshrd.btbrestfulapidemo.utils.reponse.Response;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



@RestController
@RequestMapping("/api/v1/student")
public class StudentRestController {

    // inject
    @Autowired
    private StudentService studentService;


    Student student = null;

    @Autowired
    StudentMapper studentMapper;

    ModelMapper modelMapper = new ModelMapper();
    @GetMapping("")
    public List<Student> getAllStudents (){


        try{
            student.getUsername();
            System.out.println(1/0);
        }catch (Exception exception){
            System.out.println("Error because error : "+exception.getMessage());

        }

       throw new NullPointerException();
    }
// Response<> handling exception complete
    @GetMapping("/getStudents")
    public ResponseEntity<?> getAllStudent(){

        List<Student> studentList = new ArrayList<>();

        HashMap<String, Object> response = new HashMap<String, Object>();

        try{
            studentList= studentService.getAllStudent();

            response.put("error",null);
            response.put("message","Retrieved students data successfully!");
            response.put("status", HttpStatus.OK);
            response.put("payload",studentList);

            return ResponseEntity.ok().body(response);

        }catch (Exception ex){

            System.out.println("Sql Exception Occur : "+ex.getMessage());
            response.put("error",ex.getMessage());
            response.put("message","Failed to retrieve data !");
            response.put("status", HttpStatus.NOT_FOUND);
            response.put("payload",null);

            return ResponseEntity.ok().body(response);
        }


    }
    @PostMapping("/register")
    public ResponseEntity<RegisterDTO> registerStudent(@RequestBody Student student){
//        HashMap<String, Object> response = new HashMap<String, Object>();

        RegisterDTO response = new RegisterDTO();
        try {

            int insertedUserId = studentService.registerStudent(student);
            System.out.println(" here is the value of the insertedUserId: "+insertedUserId);
            Student findStudentByID = studentService.findStudentByID(insertedUserId);


//            response.put("error",null);
//            response.put("message","Retrieved students data successfully!");
//            response.put("status", HttpStatus.OK);
//            response.put("payload",findStudentByID);

//            response = findStudentByID;
// normal mapping through the dto class
//            response.setUsername(findStudentByID.getUsername());
//            response.setGender(findStudentByID.getGender());response.setUsername(findStudentByID.getUsername());
//

//            map from mapstruc t

//            response = studentMapper.mapStudentToResponse(student);

            response = modelMapper.map(student,RegisterDTO.class);
            return ResponseEntity.ok().body(response);
        }catch (Exception ex){

            System.out.println("Exception ocruss: "+ex.getMessage());
//            response.put("error",ex.getMessage());
//            response.put("message","Failed Successfully!");
//            response.put("status", HttpStatus.NOT_FOUND);
//            response.put("payload",null);

            return ResponseEntity.ok().body(response);
        }
    }


//    NExt version

    @GetMapping("/find-all-students")
    public Response<List<Student>> findAllStudents(@RequestParam(defaultValue = "10") int limit,@RequestParam(defaultValue = "1") int page,@RequestParam(defaultValue = "") String filter){

        List<Student> studentList = new ArrayList<>();

        try{
           // studentList=studentService.getAllStudent();
            Paging paging =  new Paging();


            int offset = (page -1) * limit ;
            paging.setPage(page);
            paging.setLimit(limit);

            // setTotalPage

            studentList= studentService.findStudentPagination(limit,offset,filter);
            return Response.<List<Student>>ok().setPayload(studentList).setSuccess(true).setError("Successfully fetching the students").setMetadata(paging);

        }catch (Exception ex){

            System.out.println("Error on fetching the students : "+ex.getMessage());
            return Response.<List<Student>>exception().setError("Exception occur during fetching the student!");

        }



    }


    @PostMapping("/add-new-student")
    public Response<RegisterDTO> insertStudent(@RequestBody Student student){
        try{
            int insertUserID = studentService.registerStudent(student);
            System.out.println("Insert Id : "+insertUserID);
            // find by id

            try {
                Student findStudentByID = studentService.findStudentByID(1000);
                RegisterDTO response = modelMapper.map(findStudentByID,RegisterDTO.class);
                return Response.<RegisterDTO>ok().setPayload(response).setSuccess(true).setError("Successfully Registered");

            }catch (Exception ex){
                System.out.println("Exception : "+ex.getMessage());
                return Response.<RegisterDTO>exception().setError(" Cannot find by id. ");

            }



        }catch (Exception ex){
            System.out.println("Error on inserting the new student : "+ex.getMessage());
            return Response.<RegisterDTO>exception().setError("Error when insert the new student. ");

        }

    }


}
