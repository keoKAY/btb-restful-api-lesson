package com.kshrd.btbrestfulapidemo.service;

import com.kshrd.btbrestfulapidemo.model.Student;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface StudentService {


    List<Student> getAllStudent();
    public  int registerStudent ( Student student);
    public Student findStudentByID(int id);
    List<Student> findStudentPagination (int limit, int offset,String filter);
}
