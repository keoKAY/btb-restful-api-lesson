package com.kshrd.btbrestfulapidemo.service.serviceImp;

import com.kshrd.btbrestfulapidemo.model.Student;
import com.kshrd.btbrestfulapidemo.repository.StudentRepository;
import com.kshrd.btbrestfulapidemo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImp implements StudentService {
    /// inject repostiory
    // three
    // field injection
    // setter injection
    // constructor injection

    @Autowired
    private StudentRepository repository;


    @Override
    public List<Student> getAllStudent() {
        return repository.getAllStudents();
    }

    @Override
    public int registerStudent(Student student) {
        return repository.registerStudent(student);
    }

    @Override
    public Student findStudentByID(int id) {
        return repository.findStudentByID(id);
    }

    @Override
    public List<Student> findStudentPagination(int limit, int offset, String filter) {
        return repository.findStudentPagination(limit,offset,filter);
    }
}
