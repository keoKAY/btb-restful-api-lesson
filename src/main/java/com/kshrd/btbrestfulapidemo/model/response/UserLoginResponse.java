package com.kshrd.btbrestfulapidemo.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserLoginResponse {

    private int id;
    private String username;
    private String gender;
    private  String bio;
    private String token; //jwt token
    private List<String>  roles;

}
