package com.kshrd.btbrestfulapidemo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class Student {
    private int id;
    private String username;
    private String gender;
    private String bio;
    private String password;
    private int course_id;
}
