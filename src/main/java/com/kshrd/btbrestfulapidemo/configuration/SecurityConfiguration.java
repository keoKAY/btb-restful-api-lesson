package com.kshrd.btbrestfulapidemo.configuration;


import com.kshrd.btbrestfulapidemo.security.UserDetailServiceImp;
import com.kshrd.btbrestfulapidemo.security.jwt.JwtEntryPoint;
import com.kshrd.btbrestfulapidemo.security.jwt.JwtTokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {


    @Autowired
    JwtEntryPoint jwtEntryPoint;

// override three methods
  @Autowired
    UserDetailServiceImp userDetailServiceImp;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        super.configure(auth);
        auth.userDetailsService(userDetailServiceImp)
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        super.configure(http);
// Doesn't have the stateless

        http.csrf().disable()
                .exceptionHandling()
                .authenticationEntryPoint(jwtEntryPoint)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .antMatcher("/api/**").authorizeRequests()
                .antMatchers("/login").permitAll().anyRequest().fullyAuthenticated();

//        This is for the jwt
        http.addFilterBefore(jwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);

    }

    @Bean
public JwtTokenFilter jwtTokenFilter(){
        return new JwtTokenFilter();
}

@Bean
public AuthenticationManager authenticationManager() throws Exception {
return  super.authenticationManager();
}

//    ignore resources
    @Override
    public void configure(WebSecurity web) throws Exception {
//        super.configure(web);
        web.ignoring()
                .antMatchers("/h2-console/**",
                        "/swagger-ui.html", "/resources/**", "/static/**", "/css/**", "/js/**", "/images/**",
                        "/resources/static/**", "/css/**", "/js/**", "/img/**", "/fonts/**",
                        "/images/**", "/scss/**", "/vendor/**", "/favicon.ico", "/auth/**", "/favicon.png",
                        "/v2/api-docs", "/configuration/ui", "/configuration/security",
                        "/webjars/**", "/swagger-resources/**", "/actuator", "/swagger-ui/**",
                        "/actuator/**", "/swagger-ui/index.html", "/swagger-ui/");
    }
}
