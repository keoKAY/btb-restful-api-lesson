package com.kshrd.btbrestfulapidemo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@AllArgsConstructor

@NoArgsConstructor
public class RegisterDTO {
    private String username;
    private  String gender;
}
