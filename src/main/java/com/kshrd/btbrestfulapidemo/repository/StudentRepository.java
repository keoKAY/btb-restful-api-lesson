package com.kshrd.btbrestfulapidemo.repository;


import com.kshrd.btbrestfulapidemo.model.AuthUser;
import com.kshrd.btbrestfulapidemo.model.Student;
import com.kshrd.btbrestfulapidemo.model.UserResponse;
import com.kshrd.btbrestfulapidemo.repository.providers.StudentProvider;
import org.apache.ibatis.annotations.*;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;

@Mapper
public interface StudentRepository {

//    For response
    @Select("select * from students where username=#{username}")
    @Results(
            {
                    @Result(property = "roles",column = "id",many = @Many(select="findRolesById"))
            }
    )
    UserResponse findByUsername (String username);




//    For authentication
@Select("select * from students where username=#{username}")
@Results(
        {
                @Result(property = "roles",column = "id",many = @Many(select="findRolesById"))
        }
)
    AuthUser findUserByUsername (String username);

@Select("select role_name from roles inner join student_role sr on roles.id = sr.role_id where user_id =#{id}")
List<String> findRolesById(int id );




















    @SelectProvider(type = StudentProvider.class, method = "getAllStudentWithPagination")
    List<Student> findStudentPagination (int limit, int offset,String filter);




    @Select("select * from students ")
    public List<Student> getAllStudents();



    @Select("insert into students(id, username,gender,bio,password,course_id)" +
            " values (#{student.id},#{student.username},#{student.gender},#{student.bio}," +
            "#{student.password},#{student.course_id}) returning id")
    public  int registerStudent (@Param("student") Student student);

    @Select("select * from students where id=#{id}")
    public Student findStudentByID(int id);
}
