package com.kshrd.btbrestfulapidemo.mapper;

import com.kshrd.btbrestfulapidemo.dto.RegisterDTO;
import com.kshrd.btbrestfulapidemo.model.Student;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface StudentMapper {
    // map request -> reponse
    RegisterDTO mapStudentToResponse(Student student);

    // map response -> request
    Student mapResponseToStudent(RegisterDTO registerDTO);



}
